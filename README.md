Commandlinetool for Canvas. Subcommand 'enroll' supports adding users to courses. \

*  Usage: canvasrobot [command] [options] 
*  Help: canvasrobot --help

*  enroll a user: canvasrobot enroll--course [coursename] --user [username] --role [role]


n.c.degroot[a.t]tilburguniversity.edu